# About

This project contains the proper setup to run a typical Drupal stack. Components:

* Apache + PHP - Web layer
* MySQL - Database layer
* CLI - execute drush commands
* Solr - Search integration


# MySQL

  If you have already data to pre-populate the MySQL database, just create an .sql folder here and add the SQL scripts inside

# Solr

  For Solr you need to create the data index folders for each usable core and give them the appropriate permissions.
  
  ```
  $> mkdir ./solr/core0/data ./solr/core0/index
  $> cd ./solr/core0 && sudo chown 8983 data index
  ```
  
  On the target machine, unprivileged docker user may be given the permission to change ownership through sudo like this:
  
  ```
  $> visudo
  user     ALL=(ALL)       NOPASSWD: /bin/chown 8983 data index
  ```
  
# Cron setup
  
  In one of the infrastructure hosts you can set-up to run CRON by calling the Drupal URLs like this:
  
  ```
  # Docker containers crontab
  0,20,40 * * * * curl -s http://site1/cron.php?cron_key=SECRET_KEY
  1,21,41 * * * * curl -s http://site2/cron.php?cron_key=SECRET_KEY
  ```
